// lab7.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <cstdio>
#include <cmath>

#define ITERATIONS 50
#define TOLERANCE 1.0e-8
#define RESIDUUM 10.0e-7


class SystemOfEquations {
private:
    double **matrixA;
    double *vectorB;
    double *vectorX;
    double *vectorXLast;
    double *vectorR;
    void residuum();
public:
    SystemOfEquations(double m[2][2], double *v, double *init);
    ~SystemOfEquations();
    void Jacob();
    void GaussSeidel();
    void SOR();
};

SystemOfEquations::SystemOfEquations(double m[2][2], double *v, double *init) {
    matrixA = new double*[2];

    for(int i = 0; i < 2; i++) {
        matrixA[i] = new double[2];

        for(int j = 0; j < 2; j++) {
            matrixA[i][j] = m[i][j];
        }
    }

    vectorB = new double[2];
    for(int i = 0; i < 2; i++) {
        vectorB[i] = v[i];
    }

    vectorX = new double[2];
    for(int i = 0; i < 2; i++) {
        vectorX[i] = init[i];
    }

    vectorXLast = new double[2];
    for(int i = 0; i < 2; i++) {
        vectorXLast[i] = init[i];
    }

    vectorR = new double[2];
    for(int i = 0; i < 2; i++) {
        vectorXLast[i] = 0.0;
    }
}

SystemOfEquations::~SystemOfEquations() {
    for(int i = 0; i < 2; i++) {
        delete [] matrixA[i];
    }

    delete [] matrixA;
    // vectors
    delete [] vectorB;
    delete [] vectorX;
    delete [] vectorXLast;
    delete [] vectorR;
}

void SystemOfEquations::residuum() {
    vectorR[0] = matrixA[0][0] * vectorX[0] + matrixA[0][1] * vectorX[1] - vectorB[0];
    vectorR[1] = matrixA[1][0] * vectorX[0] + matrixA[1][1] * vectorX[1] - vectorB[1];
}
void SystemOfEquations::Jacob() {
    printf("Metoda Jacobiego:\n");
	printf("  n\t\tx\t\t est N(x0,x1)\t\t     || rn ||\n");
	printf("_____________________________________________________________________________\n");
    int iterations;
	double estN_x0 = 0.0;
	double estN_x1 = 0.0;
    for(iterations = 0; iterations < ITERATIONS; iterations++) {
        residuum();
        if(abs(vectorR[0]) < RESIDUUM && abs(vectorR[1]) < RESIDUUM) {
            break;
        }

		if(iterations == 0) {
			printf("%3d %10.6lf | %10.6lf  %10.6lf | %10.6lf  %10.6lf | %10.6lf\n", iterations, vectorX[0], vectorX[1], estN_x0, estN_x1, abs(vectorR[0]), abs(vectorR[1]));
		}

        double x0 = vectorB[0] / matrixA[0][0] - matrixA[0][1] * vectorX[1] / matrixA[0][0];
        double x1 = vectorB[1] / matrixA[1][1] - matrixA[1][0] * vectorX[0] / matrixA[1][1];
        vectorX[0] = x0;
        vectorX[1] = x1;
		estN_x0 = abs(vectorX[0] - vectorXLast[0]);
		estN_x1 = abs(vectorX[1] - vectorXLast[1]);
		printf("%3d %10.6lf | %10.6lf  %10.6lf | %10.6lf  %10.6lf | %10.6lf\n", iterations, vectorX[0], vectorX[1], estN_x0, estN_x1, abs(vectorR[0]), abs(vectorR[1]));
        if(estN_x0 < TOLERANCE && estN_x1 < TOLERANCE) {
            break;
        } else {
            vectorXLast[0] = vectorX[0];
            vectorXLast[1] = vectorX[1];
        }
    }

    residuum();
    printf("%3d %10.6lf | %10.6lf  %10.6lf | %10.6lf  %10.6lf | %10.6lf\n", iterations, vectorX[0], vectorX[1], estN_x0, estN_x1, abs(vectorR[0]), abs(vectorR[1]));
	printf("_____________________________________________________________________________\n");
}

void SystemOfEquations::GaussSeidel() {
	double estN_x0 = 0.0;
	double estN_x1 = 0.0;
    printf("Metoda Gaussa-Seidela:\n");
	printf("  n\t\tx\t\t est N(x0,x1)\t\t     || rn ||\n");
	printf("_____________________________________________________________________________\n");
    int iterations;
    for(iterations = 0; iterations < ITERATIONS; iterations++) {
        residuum();
        if(abs(vectorR[0]) < RESIDUUM && abs(vectorR[1]) < RESIDUUM) {
            break;
        }

        if(iterations == 0) {
			printf("%3d %10.6lf | %10.6lf  %10.6lf | %10.6lf  %10.6lf | %10.6lf\n", iterations, vectorX[0], vectorX[1], estN_x0, estN_x1, abs(vectorR[0]), abs(vectorR[1]));
		}
        vectorX[0] = vectorB[0] / matrixA[0][0] - matrixA[0][1] * vectorX[1] / matrixA[0][0];
        vectorX[1] = vectorB[1] / matrixA[1][1] - matrixA[1][0] * vectorX[0] / matrixA[1][1];

		estN_x0 = abs(vectorX[0] - vectorXLast[0]);
		estN_x1 = abs(vectorX[1] - vectorXLast[1]);
		printf("%3d %10.6lf | %10.6lf  %10.6lf | %10.6lf  %10.6lf | %10.6lf\n", iterations, vectorX[0], vectorX[1], estN_x0, estN_x1, abs(vectorR[0]), abs(vectorR[1]));

        if(estN_x0 < TOLERANCE && estN_x1 < TOLERANCE) {
            break;
        } else {
            vectorXLast[0] = vectorX[0];
            vectorXLast[1] = vectorX[1];
        }
    }

    residuum();
    printf("%3d %10.6lf | %10.6lf  %10.6lf | %10.6lf  %10.6lf | %10.6lf\n", iterations, vectorX[0], vectorX[1], estN_x0, estN_x1, abs(vectorR[0]), abs(vectorR[1]));
	printf("_____________________________________________________________________________\n");
}

void SystemOfEquations::SOR() {
	double estN_x0 = 0.0;
	double estN_x1 = 0.0;

    printf("Metoda SOR (omega = 0.5):\n");
	printf("  n\t\tx\t\t est N(x0,x1)\t\t     || rn ||\n");
	printf("_____________________________________________________________________________\n");

    double omega = 0.5;

    int iterations;
    for(iterations = 0; iterations < ITERATIONS; iterations++) {
        residuum();
        if(abs(vectorR[0]) < RESIDUUM && abs(vectorR[1]) < RESIDUUM) {
            break;
        }

        if(iterations == 0) {
			printf("%3d %10.6lf | %10.6lf  %10.6lf | %10.6lf  %10.6lf | %10.6lf\n", iterations, vectorX[0], vectorX[1], estN_x0, estN_x1, abs(vectorR[0]), abs(vectorR[1]));
		}
        vectorX[0] = (1.0 - omega) * vectorX[0] + omega / matrixA[0][0] * (vectorB[0] - matrixA[0][1] * vectorX[1]);
        vectorX[1] = (1.0 - omega) * vectorX[1] + omega / matrixA[1][1] * (vectorB[1] - matrixA[1][0] * vectorX[0]);

		estN_x0 = abs(vectorX[0] - vectorXLast[0]);
		estN_x1 = abs(vectorX[1] - vectorXLast[1]);
		printf("%3d %10.6lf | %10.6lf  %10.6lf | %10.6lf  %10.6lf | %10.6lf\n", iterations, vectorX[0], vectorX[1], estN_x0, estN_x1, abs(vectorR[0]), abs(vectorR[1]));
        if(abs(vectorX[0] - vectorXLast[0]) < TOLERANCE && abs(vectorX[1] - vectorXLast[1]) < TOLERANCE) {
            break;
        } else {
            vectorXLast[0] = vectorX[0];
            vectorXLast[1] = vectorX[1];
        }
    }

    residuum();
    printf("%3d %10.6lf | %10.6lf  %10.6lf | %10.6lf  %10.6lf | %10.6lf\n", iterations, vectorX[0], vectorX[1], estN_x0, estN_x1, abs(vectorR[0]), abs(vectorR[1]));
	printf("_____________________________________________________________________________\n");

}

int main() {
    double matrix[2][2] = {{5.0, 1.0}, {4.0, 10.0}};
    double vectorB[2] = {49.0, 30.0};
    double init[2] = {1.0, 1.0};
    
    // Wypisanie wprowadzonych danych
    printf("A = [%lf, %lf; %lf, %lf]\n", matrix[0][0], matrix[0][1], matrix[1][0], matrix[1][1]);
    printf("b = [%lf; %lf]\n", vectorB[0], vectorB[1]);


    // Obliczenie układu równań liniowych trzeba metodami
    SystemOfEquations(matrix, vectorB, init).Jacob();
    SystemOfEquations(matrix, vectorB, init).GaussSeidel();
    SystemOfEquations(matrix, vectorB, init).SOR();
	system("PAUSE");
    return 0;
}
